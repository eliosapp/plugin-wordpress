<?php

function COST_function_notification()
{

    $response = array(
        "Connection"    => "Success"
    );
    $res = new WP_REST_Response($response);
    $res->set_status(200);
    return [$res];
}

function COST_add_notifitacion_endpoint()
{
    register_rest_route('cost/v1', '/notification/', array(
        'methods'   =>  'GET',
        'callback'  =>  'COST_function_notification'
    ));
}


add_action('rest_api_init', 'COST_add_notifitacion_endpoint');
