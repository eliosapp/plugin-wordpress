<?php

/**
 * COST_script hook en wp_footer
 * 
 * @access public
 * @return void
 */
function COST_script()
{
    if(Elios_isLogin()){
    ?>
    <script>
        COST_script = {
            "type"  : "COST_script",
            "api"   : "<?=COST_location_url?>action.php",
            "get"   : <?=json_encode($_GET)?>,
            "ip"    : "<?=COST_getIpCliente()?>"
        }
        function getIP(json) {
            COST_script.ip = json.ip
        }
    </script>

    <script src="<?=COST_location_url?>js/api.js"></script>
    <?php
    }
}
add_action( 'wp_footer', 'COST_script', 10 );



/**
 * COST_order_send envia informacion de la orden al api
 * 
 * @access public
 * @return void
 */
function COST_order_send($order_id)
{
    $order = wc_get_order( $order_id );
    $total_weight = 0;
    $user = get_user_by('id', $order->get_user_id());
    $products = array();
    foreach ( $order->get_items() as $item_id => $item ) {
        $product_id = $item->get_product_id();
        $variation_id = $item->get_variation_id();
        $product = $item->get_product();
        $name = $item->get_name();
        $quantity = $item->get_quantity();
        $subtotal = $item->get_subtotal();
        $total = $item->get_total();
        $tax = $item->get_subtotal_tax();
        $taxclass = $item->get_tax_class();
        $taxstat = $item->get_tax_status();
        $allmeta = $item->get_meta_data();
        $somemeta = $item->get_meta( '_whatever', true );
        $type = $item->get_type();

        $product = wc_get_product( $product_id );
        $total_weight += $product->get_weight();
        $newProduct =  array(
            "variation_id"  => $variation_id,
            "subtotal"      => $$subtotal,
            "total"         => $total,
            "sku"           => $product->get_sku(),
            "permalink"     => get_permalink( $product->get_id() ),

            "weight"        => $product->get_weight(),
            "length"        => $product->get_length(),
            "width"         => $product->get_width(),
            "height"        => $product->get_height(),

            "id"                    => $product_id,
            "admin_graphql_api_id"  => get_edit_post_link($product_id),
            "fulfillable_quantity"  => $quantity,
            "name"                  => $product->get_name(),
            "price"                 => $product->get_price(),
            "product_id"            => $product_id,
            "properties"            => get_post_meta($product_id),
            "quantity"              => $quantity,
            "requires_shipping"     => !$product->is_virtual(),
            "sku"                   => $product->get_sku(),
            "taxable"               => $product->get_tax_status(),
            "title"                 => $product->get_name(),
        );
        if ($variation_id != null) {
            $variation = wc_get_product( $variation_id );
            
            $newProduct["variant_id"] = $variation_id;
            $newProduct["variant_title"] = $variation->get_name();
        }
        $products[] = $newProduct;
    }
    $tax_lines = [];
    foreach ( $order->get_tax_totals() as $rate_code => $tax ) {
        $tax_lines[] = array(
            "price"     => $tax->amount,
            "rate"      => $tax->rate_id,
            "title"     => $tax->label,
        );
    }
    $json = array(
        "type"                      => "orders-paid",
        "host"                      => $_SERVER['HTTP_HOST'],
        "id"                        => $order_id,
        "admin_graphql_api_id"      => $order->get_view_order_url(),
        "browser_ip"                => $order->get_customer_ip_address(),
        "client_details"            => array(
            "browser_ip"            => $order->get_customer_ip_address(),
            "user_agent"            => $order->get_customer_user_agent()
        ),
        "contact_email"             => $order->get_billing_email(),
        "created_at"                => $order->get_date_created(),

        "currency"                  => $order->get_currency(),
        "current_subtotal_price"    => $order->get_subtotal(),
        "current_total_discounts"   => $order->get_discount_total(),
        "current_total_price"       => $order->get_total(),
        "current_total_tax"         => $order->get_total_tax(),
        
        "discount_codes"            => $order->get_coupon_codes(),
        "email"                     => $order->get_billing_email(),
        "financial_status"          => $order->get_status(),
        "gateway"                   => $order->get_payment_method_title(),
        "name"                      => $order_id,
        "note"                      => $order->get_customer_note(),
        "order_number"              => $order_id,
        "order_status_url"          => $order->get_view_order_url(),
        "phone"                     => $order->get_billing_phone(),
        "presentment_currency"      => $order->get_currency(),
        "processed_at"              => $order->get_date_paid(),
        "subtotal_price"            => $order->get_subtotal(),
        "tax_lines"                 => $tax_lines,
        "total_discounts"           => $order->get_discount_total(),
        "total_line_items_price"    => $order->get_subtotal(),
        "total_price"               => $order->get_total(),
        "total_tax"                 => $order->get_total_tax(),
        "total_weight"              => $total_weight,
        "updated_at"                => $order->get_date_modified(),
        "billing_address"           => array(
            "first_name"            => $order->get_billing_first_name(),
            "address1"              => $order->get_billing_address_1(),
            "phone"                 => $order->get_billing_phone(),
            "city"                  => $order->get_billing_city(),
            "zip"                   => $order->get_billing_postcode(),
            "province"              => $order->get_billing_state(),
            "country"               => $order->get_billing_country(),
            "last_name"             => $order->get_billing_last_name(),
            "address2"              => $order->get_billing_address_2(),
            "company"               => $order->get_billing_company(),
            "name"                  => $order->get_billing_first_name()." ".$order->get_billing_last_name(),
        ),
        "customer"                  => array(
            "id"                    => $order->get_user_id(),
            "email"                 => $order->get_billing_email(),
            "created_at"            => $order->get_date_created(),
            "updated_at"            => $order->get_date_modified(),
            "first_name"            => $order->get_billing_first_name(),
            "last_name"             => $order->get_billing_last_name(),
            "phone"                 => $order->get_billing_phone(),
        ),
        "line_items"                => $products,
        "shipping_address"          => array(
            "first_name"            => $order->get_shipping_first_name(),
            "address1"              => $order->get_shipping_address_1(),
            "phone"                 => $order->get_billing_phone(),
            "city"                  => $order->get_shipping_city(),
            "zip"                   => $order->get_shipping_postcode(),
            "province"              => $order->get_shipping_state(),
            "country"               => $order->get_shipping_country(),
            "last_name"             => $order->get_shipping_last_name(),
            "address2"              => $order->get_shipping_address_2(),
            "company"               => $order->get_shipping_company(),
            "name"                  => $order->get_shipping_first_name()." ".$order->get_shipping_last_name(),
        ),
    );
    $api = new COST_api();

    $r = $api->sendLead($json);

    update_post_meta($order_id,"sendStartscoin",json_encode($json));
    update_post_meta($order_id,"respondStartscoin",$r);
}


/**
 * COST_order_status_processing hook de status processing ordenes
 * 
 * @access public
 * @return void
 */
function COST_order_status_processing($order_id) {
    if(Elios_isLogin()){
        COST_order_send($order_id);
    }
}
add_action('woocommerce_order_status_processing',   'COST_order_status_processing' , 10, 1); 


/**
 * COST_add_cart hook de status change cart
 * 
 * @access public
 * @return void
 */
function COST_add_cart($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data)
{
    if(Elios_isLogin()){
        global $woocommerce;
        $items = $woocommerce->cart->get_cart();
        $user_id = get_current_user_id();
        $user_info = get_userdata($user_id);
        $line_items = [];
        foreach($items as $item => $values) { 
            $_product_id = $values['product_id'];
            $_variation_id = $values['variation'];
            $_product =  wc_get_product( $_product_id );
            
            $newLineItem = array(
                "presentment_title"         => $_product->get_name(),
                "product_id"                => $_product_id,
                "properties"                => get_post_meta($product_id),
                "quantity"                  => $values['quantity'],
                "requires_shipping"         => !$_product->is_virtual(),
                "sku"                       => $_product->get_sku(),
                "taxable"                   => $_product->get_tax_status(),
                "title"                     => $_product->get_name(),
                "price"                     => $_product->get_price()
            );
            if ($_variation_id != null) {
                $_variation =  wc_get_product( $_variation_id );
                
                $newLineItem["presentment_variant_title"] = $_variation->get_name();
                $newLineItem["variant_id"] = $_variation_id;
                $newLineItem["variant_title"] = $_variation->get_name();
                $newLineItem["variant_price"] = $_variation->get_price();
            }
            $line_items[] = $newLineItem;
        }
        $product = wc_get_product( $product_id );
        $json = array(
            "ip"                    => COST_getIpCliente(),
            
            "type"                  => "checkouts-update",
            "host"                  => $_SERVER['HTTP_HOST'],
            "updated_at"            => date("c"),
            "currency"              => get_woocommerce_currency(),
            "user_id"               => $user_id,
            "phone"                 => get_user_meta($user_id,'billing_phone',true),
            "line_items"            => $line_items,
            "billing_address"       => array(
                "first_name"        => get_user_meta($user_id,'billing_first_name',true),
                "address1"          => get_user_meta($user_id,'billing_address_1',true),
                "phone"             => get_user_meta($user_id,'billing_phone',true),
                "city"              => get_user_meta($user_id,'billing_city',true),
                "zip"               => get_user_meta($user_id,'billing_postcode',true),
                "province"          => get_user_meta($user_id,'billing_state',true),
                "country"           => get_user_meta($user_id,'billing_country',true),
                "last_name"         => get_user_meta($user_id,'billing_last_name',true),
                "address2"          => get_user_meta($user_id,'billing_address_2',true),
                "company"           => get_user_meta($user_id,'billing_company',true),
                "name"              => get_user_meta($user_id,'billing_first_name',true)." ".get_user_meta($user_id,'billing_last_name',true),
            ),
            "shipping_address"      => array(
                "first_name"        => get_user_meta($user_id,'shipping_first_name',true),
                "address1"          => get_user_meta($user_id,'shipping_address_1',true),
                "city"              => get_user_meta($user_id,'shipping_city',true),
                "zip"               => get_user_meta($user_id,'shipping_postcode',true),
                "province"          => get_user_meta($user_id,'shipping_state',true),
                "country"           => get_user_meta($user_id,'shipping_country',true),
                "last_name"         => get_user_meta($user_id,'shipping_last_name',true),
                "address2"          => get_user_meta($user_id,'shipping_address_2',true),
                "company"           => get_user_meta($user_id,'shipping_company',true),
                "name"              => get_user_meta($user_id,'shipping_first_name',true)." ".get_user_meta($user_id,'shipping_last_name',true),
            ),
            "customer"              => array(
                "id"                => $user_id,
                "email"             => $user_info->user_email,
                "created_at"        => $user_info->user_registered,
                "first_name"        => get_user_meta($user_id,'billing_first_name',true),
                "last_name"         => get_user_meta($user_id,'billing_last_name',true),
                "phone"             => get_user_meta($user_id,'billing_phone',true),
                "default_address"   => array(
                    "customer_id"       => $user_id,
                    "first_name"        => get_user_meta($user_id,'billing_first_name',true),
                    "address1"          => get_user_meta($user_id,'billing_address_1',true),
                    "phone"             => get_user_meta($user_id,'billing_phone',true),
                    "city"              => get_user_meta($user_id,'billing_city',true),
                    "zip"               => get_user_meta($user_id,'billing_postcode',true),
                    "province"          => get_user_meta($user_id,'billing_state',true),
                    "country"           => get_user_meta($user_id,'billing_country',true),
                    "last_name"         => get_user_meta($user_id,'billing_last_name',true),
                    "address2"          => get_user_meta($user_id,'billing_address_2',true),
                    "company"           => get_user_meta($user_id,'billing_company',true),
                    "name"              => get_user_meta($user_id,'billing_first_name',true)." ".get_user_meta($user_id,'billing_last_name',true),
                )
            )
        );

        $api = new COST_api();

        $r = $api->sendLead($json);
    }
}

add_action('woocommerce_add_to_cart', 'COST_add_cart', 20, 6);