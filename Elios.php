<?php
/*
Plugin Name: Elios
Plugin URI: https://startscoinc.com/es/
Description: Plugin para Elios
Author: Startsco
Version: 1.21.10.15.1
Author URI: https://startscoinc.com/es/#
License: 
*/

define("ELIOSMODE","DEV");


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/eliosapp/plugin-wordpress/',
	__FILE__,
	'elios'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch(ELIOSMODE == "DEV" ? 'develop' : 'master');

/**
 * Require admin
 * Is field for requiere fields
 */
define("prefix_COST", "COST");
define("COST_location_path", plugin_dir_path(__FILE__));
define("COST_location_url", plugin_dir_url(__FILE__));
define("COST_api", "https://pixeltracking.startscoinc.com" . (ELIOSMODE == "DEV" ? "/dev":"") . "/api/v2/");
define("EliosUrl", ELIOSMODE == "DEV" ? "https://elios-dev.vercel.app/":"https://app.eliosanalytics.com/");


require_once plugin_dir_path(__FILE__) . 'functions.php';
require_once plugin_dir_path(__FILE__) . 'api.php';
require_once plugin_dir_path(__FILE__) . 'hook.php';
require_once plugin_dir_path(__FILE__) . 'endpoint.php';
require_once plugin_dir_path(__FILE__) . 'optionPage.php';
