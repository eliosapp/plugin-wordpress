# Elios 
Contributors: franciscoblanco
Tags: analytics, Colombia
License: GPLv2 (or later)
License URI: http://www.gnu.org/licenses/gpl-2.0.html

# Description  
Elios es un plugin para wordpress, compatible con woocommerce, que te permite analizar el flugo de usuarios de tu pagina web por medio de [App Elios](https://app.eliosanalytics.com/).

# Installation  
Para installar Elios debe descargarlo de Wordpress.org.

# Configuracion  
Obtenga su key de [App Elios](https://app.eliosanalytics.com/) y agréguela en el apartado de Elios en tu sitio web.