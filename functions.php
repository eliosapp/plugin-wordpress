<?php
/**
 * getIpCliente return ip of cliente
 *
 * @access public
 * @return string
 */
function COST_getIpCliente()
{
    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
/**
 * Elios_isLogin return true o false dependiedo si el plugin esta login
 *
 * @access public
 * @return boolean
 */
function Elios_isLogin()
{
    $r = get_option('Elios_login');
    return $r === "ok";
}