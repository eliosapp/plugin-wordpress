<?php
function Elios_create_menu() {

	//create new top-level menu
	add_menu_page('Elios Settings', 'Elios', 'administrator', __FILE__, 'Elios_settings_page' , plugins_url('/img/elios.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_Elios_settings' );
}
add_action('admin_menu', 'Elios_create_menu');


function register_Elios_settings() {
	//register our settings
	register_setting( 'Elios-settings-group', 'new_option_name' );
	register_setting( 'Elios-settings-group', 'some_other_option' );
	register_setting( 'Elios-settings-group', 'option_etc' );
}

function Elios_settings_page() {
    if(isset($_POST) && isset($_POST['key'])){
        update_option('Elios_settings',json_encode($_POST));
    }
    $setting = get_option('Elios_settings');
    if($setting === false || $setting ===""){
        $setting = '{"key":"","host":""}';
    }
    $setting = json_decode($setting,true);
    $api = new COST_api();

    $r = $api->login();
    $r = json_decode($r,true);
    update_option('Elios_login',$r['type']);
    ?>
    <div class="contentElios">
        <div class="contentImg">
            <img src="<?=plugins_url('/img/elios_200.png', __FILE__)?>" alt="Elios">
            <img src="<?=plugins_url('/img/eliosTitle.png', __FILE__)?>" alt="Elios">
        </div>
        <div class="contentForm">
            <form method="post">
                <input type="hidden" name="host" id="host" value="<?=$_SERVER['SERVER_NAME']?>"/>
                <label for="key" class="contentInputElios">
                    Key
                    <input type="password" name="key" id="key" value="<?=$setting['key']?>" class="inputElios"/>
                </label>
                <input type="submit" value="Save" class="inputSubmitElios"/>
                <p class="respond">
                    <span class="respond_<?=$r['type']?>"><?=$r['msj']?></span>
                    <?php
                        if( $r['type'] == "ok" ){
                            ?>
                            <br>
                            <a href="<?=EliosUrl?>" target="_blanck">
                                Go to Elios
                            </a>
                            <?php
                        }
                    ?>
                </p>
            </form>
        </div>
    </div>
    <style>
        .dNone{
            display:none !important;
        }
        .contentElios{
            max-width:550px;
            margin:20px auto;
            padding: 50px;
            background:#fff;
            border-radius:20px;
            box-shadow: 1px 1px 10px 0px hsl(0deg 0% 0% / 20%);
        }
        .contentImg{
            margin-bottom:25px;
            text-align: center;
        }
        .contentInputElios{
            display:block;
            width:100%;
            max-width:450px;
            margin:auto;
            margin-bottom:20px;
            font-size:25px;
        }
        .inputElios.inputElios.inputElios{
            display:block;
            width:100%;
            padding: 5px 20px;
            font-size:20px;
            margin-top: 10px;
            border-radius:20px;
            outline:none;
        }
        .inputSubmitElios{
            display:block;
            width:100%;
            max-width :200px;
            padding:10px;
            font-size:20px;
            margin:auto;
            cursor:pointer;
            border-radius:20px;
            --color-3   : #B5DCFF;
            --color-4   : #A7B5FF;
            background: linear-gradient(to right,var(--color-3) 0%,var(--color-4) 100%);
            border:0;
            color:#fff;
            text-transform:uppercase;
            outline:none;
        }
        .btnLogin{
            max-width :100%;
            margin: 10px 0;
            display:flex;
            align-items:center;
            justify-content: center;
        }
        .btnLogin img{
            margin: 0 5px;
        }
        .google{
            color:#616161;
            background:#ffffff;
            border:1px solid;
        }
        .facebook{
            background:#4267b2;
        }
        .facebook img{
            border-radius:5px;
        }
        .respond{
            text-align:center;
        }
        .respond_ok{
            color:green;
        }
        .respond_error{
            color:red;
        }
    </style>
    <?php 
}