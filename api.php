<?php

class COST_api {
    private $KEY = '';
    public function __construct($atts = array())
    {
        $setting = get_option('Elios_settings');
        if($setting === false || $setting ===""){
            $setting = '{"key":"","host":""}';
        }
        $setting = json_decode($setting,true);
        $this->KEY = $setting['key'];
    }
    private function request($json, $method , $rute){
        if($this->KEY == ''){
            return '{"type":"error","msj":"Key required"}';
        }
        $data = $json;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => COST_api.$rute,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$this->KEY,
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
    }
    public function login()
    {
        $cms = (class_exists( 'WooCommerce' ))?"woocommerce":"wordpress";
        return $this->request(
            array(
                "host"=>$_SERVER['SERVER_NAME'],
                "cms"=>$cms,
            ),
            'POST',
            'sites'
        );
    }
    public function sendLead($data)
    {
        return $this->request(
            $data,
            'POST',
            'leads?host='.$_SERVER['SERVER_NAME'],
        );
    }
}