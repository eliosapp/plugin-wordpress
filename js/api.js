(function() {
    const Elios = {
        url : "https://pixeltracking.startscoinc.com",
        log : false,
        dateTimeInital : 0,
        scrollMax : 0,
        get : {},
        ip : {},
        key : "",
        loadIp : (res = () => {}) => {
            if(COST_script.ip==""){
                setTimeout(()=>{
                    Elios.loadIp(res)
                },1000)
                return
            }
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            
            fetch(`https://www.iplocate.io/api/lookup/${COST_script.ip}`, requestOptions)
            .then(response => response.json())
            .then(result =>{
                console.log(result);
                Elios.ip = {
                    "ipAddress": COST_script.ip,
                    "continentName": result.continent,
                    "countryCode": result.country_code,
                    "countryName": result.country,
                    "stateProv": result.subdivision,
                    "city": result.city
                }
                res()
            })
            .catch(error => console.log('error', error));
        },
        loadGet : () => {
            Elios.get = COST_script.get
        },
        onClick : () => {
            document.body.addEventListener('click', (e) => {
                var target = e.target
                const eventData = {
                    type   : "click",
                    target  : target,
                    id      : target.id,
                    class   : target.classList.value,
                    href    : target.href,
                };
                Elios.send(eventData)
            });        
        },
        onBeforeunload : () =>{
            Elios.dateTimeInital = (new Date()).getTime()  

            window.addEventListener('beforeunload',(e)=>{
                timeOnWebstiste = (new Date()).getTime() - Elios.dateTimeInital
                timeOnWebstiste = Elios.parseTime(timeOnWebstiste)
                var eventData = {
                    type   : "Time on webstiste",
                    time  : timeOnWebstiste,
                };
                Elios.send(eventData)
        
                var eventData = {
                    type   : "Scroll",
                    target  : Elios.scrollMax,
                };
                Elios.send(eventData)
            });
        },
        onScroll : () => {
            window.addEventListener("scroll", (e) => {
                if(scrollY > Elios.scrollMax)
                    Elios.scrollMax = scrollY
            });        
        },
        onSubmit : () => {
            var Elios_form  = document.querySelectorAll('form')

            for (var i = 0; i < Elios_form.length; i++) {
                Elios_form[i].addEventListener('submit',(e)=>{
                    target = e.target
                    inputs = e.srcElement
                    var eventData = {
                        type    : "Form Submit",
                        inputs  : {},
                        id      : (target.id!=null && target.id!=undefined && target.id!="")?target.id:target.name,
                        class   : target.classList.value,
                        href    : (target.href!=null && target.href!=undefined && target.href!="")?target.href:target.action,
                    };
                    for (var j = 0; j < inputs.length; j++) {
                        input = inputs[j]
                        eventData.inputs[input.name] = input.value
                    }
                    Elios.send(eventData)
                })
            }
        },
        load : () => {
            Elios.loadIp(()=>{
                Elios.loadGet()
                Elios.onClick()
                Elios.onBeforeunload()
                Elios.onScroll()
                Elios.onSubmit()
    
                if (Elios.log) {
                    console.log("Load Elios");
                }
                Elios.send({type:"load"})
            })
        },
        parseTime : (s) => {
            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;
        
            return hrs + ':' + mins + ':' + secs + '.' + ms;
        },
        send : (event = {}) => {
            const json = {
                "userAgent"     :navigator.userAgent,
                "platform"      :navigator.platform,
                "host"          :window.location.host,
                "pageUrl"       :window.location.href,
                ...Elios.ip,
                "get"           :Elios.get,
                "event"         :event,
            }

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
    
            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(json),
                redirect: 'follow'
            };
            if (Elios.log) {
                console.log("SEND",json)
            }
            fetch(COST_script.api, requestOptions)
                .then(response => response.text())
                .then(result =>{
                    result = JSON.parse(result)
                    if (Elios.log) {
                        console.log("RESULT",result)
                    }
                })
                .catch(error => console.log('error', error));

        }
    }
    Elios.load()
})()
